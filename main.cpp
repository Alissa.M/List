#include "List.h"
#include <iostream>
#include <string>
#include <cstdlib>
using namespace std;

void Init(ForwardList* list, int argc, char*argv[]);
void ClearBuffer();

int main(int argc, char* argv[]) {
	setlocale(0, "");
	ForwardList list;
	Init(&list, argc, argv);
	
    for(;;) {
        cout << "viberite odny iz operaziy :" << endl;
        cout << "1. Raspechatat' spisok" << endl;
        cout << "2. Dobavit' elementy v spisok" << endl;
        cout << "3. Udalit' element" << endl;
        cout << "4. Naity pozicii elementov" << endl;
        cout << "5. Zamenit' odin element na drugoi" << endl;
        cout << "6. Otsortirovat' elementy spiska" << endl;
        cout << "7. Zaverschit' rabotu programmy" << endl;

		int N;

		while(!(cin >> N) || N <= 0 || N > 7) {
			ClearBuffer();
			cout << "Error: vyberite nomer ot 1 do 7" << endl;
		}

		switch (N) {
		case 1: {
			if (!list.Print())
				cout << "spisok pust" << endl;
			break;
		}
		case 2: {
			string s;
			cout << "vvedite elementy:" << endl;
			ClearBuffer();
			getline(cin, s);
			ClearBuffer();
			while (s.find(' ', s.length() - 1) != -1) s.erase(s.length() - 1, 1);
			int k = s.find(' ');
			while (k != -1) {
				string str = s.substr(0, k);
				list.Add(atoi(str.c_str()));
				s.erase(0, k + 1);
				k = s.find(' ');
			}
			list.Add(atoi(s.c_str()));
			break;
		}
		case 3: {
			cout << "vvedite znachenie elementa: ";
			int x;
			cin >> x;
			if (!list.Remove(x))
				cout << "element " << x << " ne naiden" << endl;
			break;
		}
		case 4: {
			cout << "vvedite znachenie elementa: ";
			int x;
			cin >> x;
			if(!list.Find(x))
				cout << "element " << x << " ne naiden" << endl<<endl;
			break;
		}
		case 5: {
			cout << "vvedite poziciu i novoe znachenie: ";
			int i, x;
			cin >> i >> x;
			if (!list.Replace(i, x))
				cout << "element s poziciey " << i << " ne suschestvuyet" << endl<<endl;
			break;
		}
		case 7: {
			cout << "vy hotite vyiti iz programmy? (y/n): ";
			string ans;
			while (true) {
				ClearBuffer();
				getline(cin, ans);
				ClearBuffer();
				if ((ans == "y") || (ans == "Y") || (ans == "Yes") || (ans == "yes") || (ans == "YES)")) {
					cout << endl << "do svidaniya!"<<endl;
					return 0;
				}
				else {
					if ((ans == "n") || (ans == "N") || (ans == "No") || (ans == "no") || (ans == "NO"))
						break;
					else {
						cout << endl;
						continue;
					}

				}
			}
		}
		}
cout<<endl;
	}
}


void Init(ForwardList * list, int argc, char*argv[])
{
	if (argc == 1)
	{
		return;
	}
	string s(argv[1]);
	if (s.find(',') == -1)
		for (int i = 1; i < argc; i++)
			list->Add(atoi(argv[i]));
	else
	{
		int k = s.find(',');
		while (k != -1)
		{
			string str = s.substr(0, k);
			list->Add(atoi(str.c_str()));
			s.erase(0, k + 1);
			k = s.find(',');
		}
		list->Add(atoi(s.c_str()));
	}
}

void ClearBuffer()
{
	cin.clear();
	cin.ignore(cin.rdbuf()->in_avail());
}