#include "List.h"
#include <iostream>
using namespace std;

int size;

ForwardList::ForwardList()
{
	first = nullptr;
	size = 0;
}

ForwardList::~ForwardList()
{
	Node*cur1 = first;
	while (cur1 != nullptr)
	{
		Node*cur2 = cur1->next;
		delete cur1;
		cur1 = cur2;
	}
	first = nullptr;
}

bool ForwardList::Print()
{
	Node* cur = first;
	if (cur == nullptr)
		return false;
	cout << cur->data;
	cur = cur->next;
	while (cur != nullptr)
	{
		cout << " -> " << cur->data;
		cur = cur->next;
	}
	cout << endl;
	return true;
}

void ForwardList::Add(int x)
{
	size++;
	if (first == nullptr)
		first = new Node{ x };
	else
	{
		Node* cur = first;
		while (cur->next != nullptr) cur = cur->next;
		cur->next = new Node{ x };
	}
}

bool ForwardList::Remove(int value)
{
	Node*cur = first;
	if (cur == nullptr) return false;
	if (cur->data == value)
	{
		first = cur->next;
		delete cur;
		return true;
	}
	while (cur->next != nullptr)
	{
		if (cur->next->data == value)
		{
			cur->next = cur->next->next;
			delete cur->next;
			return true;
		}
		cur = cur->next;
	}
	return false;
}

bool ForwardList::Find(int value)
{
	Node*cur = first;
	int i = 0;
	bool flag = false;
	while (cur != nullptr)
	{
		if (cur->data == value)
		{
			cout << i << ' ';
			flag = true;
		}
		i++;
		cur = cur->next;
	}
	cout << endl;
	return flag;
}

bool ForwardList::Replace(int index, int value)
{
	Node*cur = first;
	int i = 0;
	while (cur != nullptr && i != index)
	{
		cur = cur->next;
		i++;
	}
	if (cur == nullptr)
		return false;
	cur->data = value;
	return true;
}

bool ForwardList::Sort()
{
	if (first == nullptr) return false;

}