class ForwardList
{
public:
	ForwardList();
	~ForwardList();
	bool Print();
	void Add(int );
	bool Remove(int );
	bool Find(int );
	bool Replace(int, int);
	bool Sort();
private:
	class Node
	{
	public:
		int data;
		Node* next;
		Node() = delete;
		Node(int data_) : Node {data_, nullptr} {}
		Node(int data_, Node* next_) : data{data_}, next{ next_ }{}
	};
	Node*first;
};